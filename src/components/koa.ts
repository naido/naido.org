import { server } from '../server'
import koaViews from 'koa-views'
import koaStatic from 'koa-static'
import koaMount from 'koa-mount'
import koaBody from 'koa-body'
import koaSession from 'koa-session'
import path from 'path'

/**
 * Initializes the KOA http server and router.
 */
export function beforeInitialize() {
    server.koa.keys = [server.config.KOA_KEYS]

    server.koa.use(
        koaMount('/public', koaStatic(path.join(__dirname, '..', 'public')))
    )
    server.koa.use(koaViews(path.join(__dirname, '..', 'views')))
    server.koa.use(koaBody())
    server.koa.use(
        koaSession(
            {
                key: 'naido:sess',
                maxAge: 5 * 24 * 60 * 60 * 1000, // 5 days
                signed: server.config.IS_DEV !== '1',
                renew: true
            },
            server.koa
        )
    )

    server.koa.use(server.router.routes())
    server.koa.use(server.router.allowedMethods())
}

export function afterInitialize() {
    const port = parseInt(server.config.WEB_PORT || '3000')

    server.koa.listen(port)
    server.logger.info(`Now listening on http://127.0.0.1:${port}/`)
}
