import { UserService } from '../interfaces'
import globby from 'globby'
import path from 'path'
import bunyan from 'bunyan'
import { DbUser } from '../models/user'

export class ServiceManager {
    services: UserService[] = []
    logger = bunyan
        .createLogger({
            name: 'naido.org'
        })
        .child({
            component: 'serviceManager'
        })

    async init() {
        await this.loadServices()
    }

    private async loadServices() {
        const files = await globby([
            path
                .join(__dirname, '..', 'user-services', '*', 'index.ts')
                .replace(/\\/g, '/'),
            path
                .join(__dirname, '..', 'user-services', '*', 'index.js')
                .replace(/\\/g, '/')
        ])

        for (let file of files) {
            try {
                this.services.push(require(file))
            } catch (err) {
                this.logger.error({ err, file }, 'Could not autoload file')
            }
        }

        this.logger.info(`Loaded ${this.services.length} services.`)
    }

    /**
     * Gets a summary of the user's services, and their status (active,
     * inactive, unavailable...)
     */
    getUserSummary(user: DbUser) {
        return Promise.all(
            this.services.map(async service => {
                const config = await this.getServiceConfiguration(
                    service.serviceInfo.slug
                )

                if (!config)
                    return {
                        slug: service.serviceInfo.slug,
                        status: 'inactive'
                    }

                const userConfig = await this.getUserServiceConfiguration(
                    user,
                    service.serviceInfo.slug
                )

                if (!userConfig)
                    return {
                        slug: service.serviceInfo.slug,
                        status: 'inactive'
                    }

                return {
                    slug: service.serviceInfo.slug,
                    status: await service.serviceActions.isAccountActive(
                        user,
                        config,
                        userConfig
                    )
                }
            })
        )
    }

    async getServiceConfiguration(serviceSlug: string) {
        return null
    }

    async getUserServiceConfiguration(user: DbUser, serviceSlug: string) {
        return null
    }
}

export const serviceManager = new ServiceManager()
