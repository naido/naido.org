import { server } from '../server'
import { Sequelize } from 'sequelize-typescript'
import { DbUser } from '../models/user'
import { DbServiceConfigurationUser } from '../models/service-configuration-user'

/**
 * Initializes the database
 */
export async function beforeInitialize() {
    server.db = new Sequelize(server.config.DATABASE_URI)

    await server.db.authenticate()

    // All models have to be added at once in order for associations to work
    server.db.addModels([DbUser, DbServiceConfigurationUser])

    await server.runHook('databaseLoaded')

    await server.db.sync({ force: true })

    const user: DbUser = DbUser.build({
        username: 'kindly',
        accessLevel: 2
    })
    await user.setPassword('abcdef')
    await user.save()
}
