import { Table, Model, Column, DataType } from 'sequelize-typescript'

@Table({
    modelName: 'ServiceConfigurationUser',
    tableName: 'service_configuration_user'
})
export class DbServiceConfigurationUser extends Model<
    DbServiceConfigurationUser
> {
    @Column({ type: DataType.STRING(64), allowNull: false })
    serviceSlug!: string

    @Column({ type: DataType.JSONB, allowNull: false })
    configuration!: object
}
