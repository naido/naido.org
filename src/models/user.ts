import { Table, Model, Column, DataType } from 'sequelize-typescript'
import { server } from '../server'
import bcrypt from 'bcrypt'
import moment from 'moment'

export enum AccessLevels {
    default = 0,
    club = 1,
    admin = 2
}

export enum MembershipStatuses {
    inactive = 0, // Jamais été activé
    suspended = 1, // Activé mais compte expiré
    active = 2 // Activé
}

@Table({
    modelName: 'User',
    tableName: 'users',
    indexes: [
        {
            unique: true,
            fields: ['username']
        }
    ]
})
export class DbUser extends Model<DbUser> {
    @Column({ type: DataType.STRING(26), allowNull: false })
    username!: string

    @Column({ type: DataType.STRING(60), allowNull: false })
    password!: string

    @Column({ type: DataType.STRING(255) })
    backupEmail!: string

    @Column({ type: DataType.DATE })
    memberUntil!: Date

    @Column({ type: DataType.INTEGER, defaultValue: AccessLevels.default })
    accessLevel!: number

    checkPassword(password) {
        return bcrypt.compare(password, this.password)
    }

    async setPassword(password) {
        this.password = await bcrypt.hash(password, 12)
    }

    hasAccess(level: AccessLevels | keyof typeof AccessLevels) {
        if (typeof level === 'string') level = AccessLevels[level]

        return level <= this.accessLevel
    }

    isAccess(level: AccessLevels | keyof typeof AccessLevels) {
        if (typeof level === 'string') level = AccessLevels[level]

        return level === this.accessLevel
    }

    membershipStatus(): MembershipStatuses {
        if (this.hasAccess('club') || moment() < moment(this.memberUntil))
            return MembershipStatuses['active']

        if (this.memberUntil) return MembershipStatuses['suspended']

        return MembershipStatuses['inactive']
    }
}
