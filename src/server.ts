import Koa from 'koa'
import Router from '@koa/router'
import bunyan from 'bunyan'
import globby from 'globby'
import path from 'path'
import { Config } from './interfaces'
import { Sequelize } from 'sequelize-typescript'
import { serviceManager } from './components/service-manager'

/**
 * Server class that should have only one instance (the one exported at the
 * bottom of this file). It initializes all components and contains references
 * to all shared objects (router, logger...)
 */
export class Server {
    config!: Config
    koa = new Koa()
    router = new Router()
    logger = bunyan.createLogger({
        name: 'naido.org'
    })
    db!: Sequelize
    modules: any[] = []

    // To be added with a `.js` suffix
    MOD_PATHS = [
        'models/*.js',
        'components/*.js',
        'middleware/*.js',
        'controllers/**/*.js'
    ]
    MOD_AUTO_HOOKS = ['beforeInitialize', 'initialize', 'afterInitialize']

    async start(config: Config) {
        this.config = config
        await this.loadFiles()
        await serviceManager.init()
    }

    private async loadFiles() {
        for (let p of this.MOD_PATHS) {
            const files = await globby(
                path.join(__dirname, p).replace(/\\/g, '/')
            )

            for (let file of files) {
                try {
                    this.modules.push(require(file))
                } catch (err) {
                    this.logger.error({ err, file }, 'Could not autoload file')
                }
            }
        }

        for (let hook of this.MOD_AUTO_HOOKS) {
            await this.runHook(hook)
        }
    }

    public async runHook(name: string, ...data: any) {
        for (let mod of this.modules) {
            if (mod && typeof mod[name] === 'function') {
                await mod[name](...data)
            }
        }
    }
}

export const server = new Server()
