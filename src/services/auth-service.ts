import { DbUser } from '../models/user'
import { Op } from 'sequelize'

export class AuthService {
    /**
     * Checks wether a given username/password combination is correct. Returns
     * the user if so.
     */
    static async login(username: string, password: string) {
        const user = await DbUser.findOne({
            where: {
                [Op.or]: {
                    username: username,
                    backupEmail: username
                }
            }
        })

        if (!user || !(await user.checkPassword(password))) return false

        return user
    }
}
