import format from 'string-format'

export class LanguageService {
    static getLanguages() {
        return ['fr']
    }

    static translate(str: string, ...args: any[]) {
        // Currently we don't translate anything at all, only format
        return format(str, ...args)
    }
}
