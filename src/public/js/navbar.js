let openButtonEl = document.getElementById('navbar_toggle')
let navbarEl = document.getElementById('navbar')

openButtonEl.addEventListener('click', () => {
    navbarEl.classList.toggle('navbar-open')
})
