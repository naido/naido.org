import { server } from './server'
import { Config } from './interfaces'

const config: Config = require('dotenv').config().parsed

server.start(config).catch(e => {
    console.error(e)
    process.exit(1)
})
