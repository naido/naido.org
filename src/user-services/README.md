# User Services

Les services utilisateurs sont les composantes du site web permettant aux utilisateurs de gérer leurs comptes sur les différents services proposés par l'hébergeur. Ils sont présents dans ce dossier.

Chaque service est un dossier, et doit comporter au minimum un fichier `index.ts`, avec comme template:

```ts
import { DbUser } from '../../models/user'

export const serviceInfo = {
    name: 'Email', // Nom affiché dans le dashboard
    slug: 'email', // Slug dans l'URL du dashboard: /services/:slug
    autoRegister: true // Un compte doit-il être créé automatiquement lorsqu'un utilisateur est inscrit (et activé) ?
}

/**
 * Only necessary when serviceInfo.autoRegister is set to `true`
 */
export const autoRegisterActions = {
    // Can this username be used to register an account?
    isAccountAvailable(user: DbUser, username: string, serviceConfiguration) {},

    // Actually do the registration
    registerAccount(
        user: DbUser,
        username: string,
        serviceConfiguration,
        userConfiguration
    ) {}
}
```
