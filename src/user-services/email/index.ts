import { UserService } from '../../interfaces'
import axios from 'axios'

function request(serviceConfiguration: any) {
    return axios.create({
        baseURL: serviceConfiguration.mailcowBaseURL,
        timeout: 5000,
        headers: {
            'X-API-Key': serviceConfiguration.mailcowApiKey
        }
    })
}

export const serviceInfo: UserService['serviceInfo'] = {
    name: 'Email',
    slug: 'email',
    autoRegister: true
}

export const serviceActions: UserService['serviceActions'] = {
    async isServiceAvailable(serviceConfiguration) {
        return true
    },

    async isAccountAvailable({ username }, serviceConfiguration) {
        const mailboxes = await request(serviceConfiguration)
            .get(
                `api/v1/get/mailbox/${username}@${serviceConfiguration.defaultDomain}`
            )
            .then(r => r.data)

        return mailboxes.length === 0
    },

    async registerAccount(
        user,
        { username, password },
        serviceConfiguration,
        userConfiguration
    ) {
        await request(serviceConfiguration).post('api/v1/add/mailbox', {
            local_part: username,
            domain: serviceConfiguration.defaultDomain,
            name: username,
            quota: serviceConfiguration.defaultMailboxQuota,
            password,
            password2: password,
            active: 1
        })
        userConfiguration.activated = true
        return true
    },

    getShortcut(user, serviceConfiguration, userConfiguration) {
        return (
            userConfiguration.activated && {
                icon: '',
                label: 'Email',
                link: serviceConfiguration.webmailURL
            }
        )
    },

    isAccountActive(user, serviceConfiguration, userConfiguration) {
        return userConfiguration.activated
    }
}
