import { server } from '../server'
import { DbUser } from '../models/user'

export function initialize() {
    server.router.use(async (ctx, next) => {
        if (ctx.session.userID) {
            ctx.state.user = await DbUser.findByPk(ctx.session.userID)
        }

        return next()
    })

    server.router.use(['/home', '/home/:page(.*)'], (ctx, next) => {
        return ctx.state.user ? next() : ctx.redirect('/auth/login')
    })
}
