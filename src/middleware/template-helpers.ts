import { server } from '../server'
import { LanguageService } from '../services/language-service'
import moment from 'moment'

export function initialize() {
    server.router.use((ctx, next) => {
        ctx.state.__ = (str: string, ...args: any[]) =>
            LanguageService.translate(str, ...args)

        ctx.state.asset = (asset: string) => `/public/${asset}`
        ctx.state.link = (str: string) =>
            ctx.params.lang ? `/${ctx.params.lang}${str}` : `${str}`
        ctx.state.request = ctx.request
        ctx.state.params = ctx.params
        ctx.state.IS_DEV = server.config.IS_DEV === '1'
        ctx.state.errors = []
        ctx.state.moment = moment

        // Redirect to the same path
        ctx.refresh = () => {
            return ctx.redirect(ctx.request.path)
        }

        return next()
    })
}
