import { server } from '../server'

/**
 * Allows for sending one-time messages to a user. These messages are
 * automatically cleared once they have been rendered once, meaning it is
 * possible to redirect to another page and still display the flash message.
 */
export function initialize() {
    server.router.use((ctx, next) => {
        if (!Array.isArray(ctx.session.flash)) ctx.session.flash = []
        ctx.state._session = ctx.session

        ctx.flash = msg => {
            ctx.session.flash.push(msg)
        }

        return next()
    })
}
