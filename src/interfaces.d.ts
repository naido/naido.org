import { DbUser } from './models/user'

export interface Config {
    /**
     * Port the web server should listen on. Defaults to 3000.
     */
    WEB_PORT: string

    /**
     * MySQL connection string, passed directly to Sequelize
     */
    DATABASE_URI: string

    /**
     * Are we running on a development setup? Either "1" for yes or something
     * else for no.
     */
    IS_DEV: string

    /**
     * Keys for KOA
     */
    KOA_KEYS: string
}

export interface UserService {
    /**
     * Gives static information about this user service
     */
    serviceInfo: {
        /**
         * Name of the service, as displayed in the website
         */
        name: string

        /**
         * Slug of the service, as displayed in the service URL
         */
        slug: string

        /**
         * Whether an account can be automatically created alongside a user account
         */
        autoRegister: boolean
    }

    /**
     * Actions that the website needs in order to integrate with the user service
     */
    serviceActions: {
        /**
         * Is the service currently available? This is displayed in the
         * dashboard, and account registration will be temporarely disabled if a
         * service with autoRegister === true is unavailable.
         *
         * Should throw with an error message if the service is unavailable.
         */
        isServiceAvailable: (
            serviceConfiguration: any
        ) => Promise<boolean> | boolean

        /**
         * To be implemented if serviceInfo.autoRegister === true. If an account
         * can be automatically created from this user account.
         *
         * Example: user has username "john". The email service will check if
         * john@naido.org is an available email. If it is not, the user will not
         * be able to register at all, since autoRegister === true and the
         * account can't be created.
         */
        isAccountAvailable?: (
            user: { username: string; name: string; backupEmail: string },
            serviceConfiguration: any
        ) => Promise<boolean> | boolean

        /**
         * To be implemented if serviceInfo.autoRegister === true. This actually
         * does the account creation thing.
         *
         * Should throw if there is an error, although that shouldn't happen.
         */
        registerAccount?: (
            user: DbUser,
            account: { username: string; password: string },
            serviceConfiguration: any,
            userConfiguration: any
        ) => Promise<boolean> | boolean

        /**
         * Gets the shortcut as displayed on the "My account" page.
         */
        getShortcut: (
            user: DbUser,
            serviceConfiguration: any,
            userConfiguration: any
        ) =>
            | {
                  icon: string
                  label: string
                  link: string
              }
            | false

        isAccountActive: (
            user: DbUser,
            serviceConfiguration: any,
            userConfiguration: any
        ) => boolean
    }
}
