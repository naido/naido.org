import { server } from '../server'

export function initialize() {
    server.router.get('/', ctx => {
        return ctx.render('pages/index.pug')
    })
}
