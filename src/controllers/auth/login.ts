import { server } from '../../server'
import { AuthService } from '../../services/auth-service'

export function initialize() {
    server.router.get('/auth/login', async ctx => {
        if (ctx.state.user) return ctx.redirect('/home')

        return ctx.render('pages/auth/login.pug')
    })

    server.router.post('/auth/login', async ctx => {
        const username = ctx.request.body.username
        const password = ctx.request.body.password

        const user = await AuthService.login(username, password)

        if (user) {
            ctx.session.userID = user.id
            return ctx.redirect('/home')
        } else {
            ctx.flash({
                type: 'error',
                message: ctx.state.__(
                    `Combinaison nom d'utilisateur/mot de passe incorrecte.`
                )
            })

            return ctx.render('pages/auth/login.pug', {
                username
            })
        }
    })
}
