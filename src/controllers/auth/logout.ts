import { server } from '../../server'

export function initialize() {
    server.router.get('/auth/logout', async ctx => {
        delete ctx.session.userID

        return ctx.redirect('/')
    })
}
