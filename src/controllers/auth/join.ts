import { server } from '../../server'
import { AuthService } from '../../services/auth-service'
import { DbUser } from '../../models/user'

export function initialize() {
    server.router.get('/auth/join', async ctx => {
        if (ctx.state.user) return ctx.redirect('/home')

        return ctx.render('pages/auth/join.pug', {
            _user: {}
        })
    })

    server.router.post('/auth/join', async ctx => {
        const body = ctx.request.body

        const user = DbUser.build({
            username: body.username,
            backupEmail: body.backupEmail || null
        })

        if (!body.password || body.password.length < 8) {
            ctx.flash({
                type: 'error',
                message: `Votre mot de passe doit être composé d'au moins 8 caractères.`
            })

            return ctx.render('pages/auth/join.pug', {
                _user: user
            })
        }

        await user.setPassword(body.password)

        try {
            await user.save()
        } catch (e) {
            let err = ''

            if (e.errors) {
                e = e.errors[0]

                if (e.path == 'username' && e.validatorKey == 'not_unique') {
                    err = "Nom d'utilisateur déjà pris."
                } else if (e.path == 'email') {
                    err = 'Adresse email invalide'
                } else {
                    err =
                        'Erreur interne. Merci de vouloir rééssayer plus tard.'
                }
            } else {
                err = 'Erreur interne. Merci de vouloir rééssayer plus tard.'
            }

            ctx.flash({
                type: 'error',
                message: err
            })

            return ctx.render('pages/auth/join.pug', {
                _user: user
            })
        }

        ctx.session.userID = user.id
        return ctx.redirect('/home')
    })
}
