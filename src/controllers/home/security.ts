import { server } from '../../server'

export function initialize() {
    server.router.get('/home/security', ctx => {
        return ctx.render('pages/home/security.pug')
    })
}
