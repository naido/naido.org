import { server } from '../../server'
import { serviceManager } from '../../components/service-manager'

export function initialize() {
    server.router.get('/home/services', async ctx => {
        const servicesSummary = await serviceManager.getUserSummary(
            ctx.state.user
        )

        const services = serviceManager.services.map(service => {
            return {
                ...servicesSummary.find(
                    ss => ss.slug === service.serviceInfo.slug
                ),
                name: service.serviceInfo.name
            }
        })

        return ctx.render('pages/home/services.pug', {
            services
        })
    })
}
