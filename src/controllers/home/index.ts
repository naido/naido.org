import { server } from '../../server'

export function initialize() {
    server.router.get('/home', ctx => {
        return ctx.render('pages/home/index.pug')
    })
}
