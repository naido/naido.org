import { server } from '../../server'
import { DbUser } from '../../models/user'

export function initialize() {
    server.router.get('/home/password', ctx => {
        return ctx.render('pages/home/password.pug')
    })

    server.router.post('/home/password', async ctx => {
        const user: DbUser = ctx.state.user
        const oldPassword = ctx.request.body.old_password
        const newPassword = ctx.request.body.new_password
        const newPassword2 = ctx.request.body.new_password2

        try {
            if (!(await user.checkPassword(oldPassword))) {
                throw Error('Mot de passe actuel invalide')
            } else if (newPassword !== newPassword2) {
                throw Error(
                    'Nouveau mot de passe et mot de passe répété différents'
                )
            } else if (newPassword.length < 8) {
                throw Error(
                    'Le nouveau mot de passe doit être composé de 8 caractères au minimum'
                )
            }
        } catch (e) {
            ctx.flash({
                type: 'error',
                message: ctx.state.__(e.message)
            })

            return ctx.refresh()
        }

        await user.setPassword(newPassword)
        await user.save()

        ctx.flash({
            type: 'success',
            message: ctx.state.__('Mot de passe changé')
        })

        return ctx.refresh()
    })
}
