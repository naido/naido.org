# Naido

Ceci est le dépôt pour [naido.org](https://naido.org/), un site qui propose des services respectueux de la vie privée.

## Organisation

Dans le dossier `src/`, il y a:

-   `components`: Code qui ne tombe pas dans les controllers/middleware, mais qui n'est pas indépendant
-   `controllers`: Controllers de pages web
-   `middleware`: Middleware KOA ou constructeurs de middleware
-   `models`: Les modèles Sequelize
-   `public`: Tout contenu copié tel quel dans `dist/public/` lors de la build et servi en HTTP sur `/public/`
-   `scss`: Code SCSS (style du site)
-   `services`: Code qui ne tombe pas dans les controllers/middleware et qui est indépendant
-   `views`: Les vues utilisés par les controllers
-   `user-services`: Les gestionnaires permettant aux utilisateurs de gérer leurs services (email, cloud, matrix, ...)

## User Services

Le panel membre de Naido est modulaire, c'est-à-dire qu'on peut y ajouter et supprimer des services. Pour installer vos propres services, il suffit de les mettre dans le dossier `src/user-services`. Pour plus de doc, voir `src/user-services/README.md`.
