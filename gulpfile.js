const gulp = require('gulp')
const ts = require('gulp-typescript')
const nodemon = require('gulp-nodemon')
const sourcemaps = require('gulp-sourcemaps')
const sass = require('gulp-sass')
const Fiber = require('fibers')
sass.compiler = require('node-sass')

const tsProject = ts.createProject('tsconfig.json')

function typescript() {
    return gulp
        .src('src/**/*.ts')
        .pipe(sourcemaps.init())
        .pipe(tsProject())
        .pipe(
            sourcemaps.write('.', {
                includeContent: false,
                sourceRoot: '../src'
            })
        )
        .pipe(gulp.dest('dist'))
}

function copy(done) {
    return gulp.parallel(
        () => gulp.src(['src/views/**/*.pug']).pipe(gulp.dest('dist/views/')),
        () => gulp.src(['src/public/**/*']).pipe(gulp.dest('dist/public/'))
    )(done)
}

function scss() {
    return gulp
        .src('src/scss/main.scss')
        .pipe(sass({ fiber: Fiber }).on('error', sass.logError))
        .pipe(gulp.dest('dist/public/'))
}

function run(done) {
    nodemon({
        script: 'dist/index.js',
        ext: 'js',
        env: { NODE_ENV: 'development' },
        delay: 1000,
        done
    })
}

function watch() {
    gulp.watch(
        'src/**/*.ts',
        {
            ignoreInitial: false
        },
        typescript
    )
    gulp.watch(
        ['src/views/**/*.pug', 'src/public/**/*'],
        {
            ignoreInitial: false
        },
        copy
    )
    gulp.watch(
        'src/scss/**/*.scss',
        {
            ignoreInitial: false
        },
        scss
    )
}

exports.typescript = typescript
exports.copy = copy
exports.watch = watch
exports.run = run
exports.scss = scss
exports.default = gulp.parallel(typescript, copy, scss)
